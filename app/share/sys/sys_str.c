
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>


#include "private_type.h"
#include "private_log.h"
#include "ipc_protocol.h"
#include "memory/sys_memory.h"

char *SYS_MacToStr(UINT8 *aucMac)
{
    static char szMac[BUF_SIZE32] = {0};

    sprintf(szMac, "%02x:%02x:%02x:%02x:%02x:%02x", aucMac[0],aucMac[1],aucMac[2],
            aucMac[3],aucMac[4],aucMac[5]);
    return szMac;
}
int GetLocalMac(OUT UINT *aucMac)
{
    int sock_mac;

    struct ifreq ifr_mac;
    char mac_addr[30];

    sock_mac = socket( AF_INET, SOCK_STREAM, 0 );
    if( sock_mac == -1)
    {
        return IPC_STATUS_FAIL;
    }

    memset(&ifr_mac,0,sizeof(ifr_mac));
    strncpy(ifr_mac.ifr_name, "eth0", sizeof(ifr_mac.ifr_name)-1);

    if( (ioctl( sock_mac, SIOCGIFHWADDR, &ifr_mac)) < 0)
    {
        return IPC_STATUS_FAIL;
    }

    memcpy(aucMac, ifr_mac.ifr_hwaddr.sa_data, MAC_ADDR_LEN);
    return IPC_STATUS_OK;
}

int SYS_StrToIp (const char *cp)
{
  int dots = 0;
  UINT addr = 0;
  UINT val = 0, base = 10;
  UINT uiIpaddr = 0;

  do
  {
      register char c = *cp;

      switch (c)
      {
          case '0': case '1': case '2': case '3': case '4': case '5':
          case '6': case '7': case '8': case '9':
            val = (val * base) + (c - '0');
            break;
          case '.':
            if (++dots > 3)
              return 0;
          case '\0':
            if (val > 255)
              return 0;
            addr = addr << 8 | val;
            val = 0;
            break;
          default:
            return 0;
      }
    } while (*cp++) ;

   if (dots < 3)
     addr <<= 8 * (3 - dots);

   uiIpaddr = htonl (addr);
   return uiIpaddr;
}

