#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>

#include "private_type.h"
#include "private_log.h"
#include "ipc_protocol.h"
#include "memory/sys_memory.h"
#include "btree/sys_btree.h"

typedef struct debug_funcs_t
{
    char *name;
    int (*func)(int argc,char **argv);
    int (*helpshow)(char *name);
}DEBUG_FUNCS_T;

/*********************************/
extern int HelpHelp(char *name);
extern int HelpCmd(int argc, char **argv);
extern int MemoryCtrHelp(char *name);
extern int MemoryCtrCmd (int argc, char **argv);
extern int BTreeCtrHelp(char *name);
extern int BTreeCtrCmd(int argc, char **argv);

/*********************************/

DEBUG_FUNCS_T applets[]=
{
  {  "help",HelpCmd, HelpHelp},
  {  "memory",MemoryCtrCmd, MemoryCtrHelp},
  {  "btree",BTreeCtrCmd, BTreeCtrHelp}
};
#if 0
#endif
typedef struct TreeTestNode_t
{
    INT *pData;                  //数据域
    struct TreeNode_t *Child[BI_TREE_CHILD_NUM];  //孩子
}BT_TEST_NODE_ST;
BT_TEST_NODE_ST g_BtreeTest;

/*==================================================================
* Function	: btree_test
* Description	: 二叉树查找性能测试函数 ---- 二叉树查找与memcpy相差62倍，
                                              所以当表项超过62个时，二叉树肯定比链表查找快
* Input Para	:
* Output Para	:
* Return Value:
==================================================================*/
void btree_test()
{
    UINT8 szCmp[96] = "dddfffsdff";
    UINT8 szCmp1[96] = "dddfffsdff333";
    BT_INDEX_ST stIndex;
    BT_TEST_NODE_ST *pstNode=NULL;
    int i = 0;

    stIndex.auiIndex[0] = 1;
    stIndex.auiIndex[1] = 1;
    stIndex.auiIndex[3] = 1;
    stIndex.uiIndexLen = 96;

    pstNode = BT_AddBTree(&g_BtreeTest, &stIndex);
    if(pstNode!= NULL)
    {
        pstNode->pData = MEM_MALLOC(sizeof(INT));
    }

    CW_SendErr("btree start %u\r\n", sys_GetTimeUsecs());
    for(i=0; i < 10000000; i++)
    {
        pstNode = BT_FindBTree(&g_BtreeTest, &stIndex);
    }
    CW_SendErr("btree end %u\r\n", sys_GetTimeUsecs());



    CW_SendErr("cmp start %u\r\n", sys_GetTimeUsecs());
    for(i=0; i < 10000000; i++)
    {
        if(memcmp(szCmp, szCmp1, sizeof(szCmp1)))
        {
            continue;
        }
    }
    CW_SendErr("cmp end %u\r\n", sys_GetTimeUsecs());

}

int BTreeCtrHelp(char *name)
{
    printf("debug btree find 12345\n");
    printf("debug btree add 12345\n");
    printf("debug btree del 12345\n");
    printf("debug btree test 111\n");
    return 0;
}
int BTreeCtrCmd(int argc, char **argv)
{
    int Index = 0;
    int Value = 0;
    if(argc < 3)
    {
        return;
    }
    Index = atoi(argv[3]);
    #if 0
    if(strcmp("find", argv[2]) == 0)
    {
        IPC_BTREE_GetValue(Index, &Value);
        printf("find index=%d, value=%d\n", Index, Value);
    }
    else if(strcmp("add", argv[2]) == 0)
    {
        IPC_BTREE_Add(Index);
    }
    else if(strcmp("del", argv[2]) == 0)
    {
        IPC_BTREE_Del(Index);
    }
    else if (strcmp("test", argv[2]) == 0)
    {
        btree_test();
    }
    #endif

    return 0;
}


#if 0
#endif
int HelpHelp(char *name)
{
    printf("debug help\n");
    return 0;
}
int HelpCmd(int argc, char **argv)
{
    const DEBUG_FUNCS_T *a;

    for (a = applets; a->name; ++a)
    {
       a->helpshow(a->name);
    }

    return 0;
}
#if 0
#endif
int MemoryCtrHelp(char *name)
{
    printf("debug memory show\n");
    printf("debug memory enable\n");
    printf("debug memory disable\n");
    return 0;
}
int MemoryCtrCmd(int argc, char **argv)
{
    if(argc < 2)
    {
        return;
    }

    if(strcmp("enable", argv[2]) == 0)
    {
        MEM_DebugConfig(TRUE);
    }
    else if(strcmp("disable", argv[2]) == 0)
    {
        MEM_DebugConfig(FALSE);
    }
    else if(strcmp("show", argv[2]) == 0)
    {
        MEM_ShowDebugInfo();
    }

    return 0;
}
#if 0
#endif

int main(int argc, char **argv)
{
    char *base = NULL;

    const DEBUG_FUNCS_T *a;
    if(argc < 2)
    {
        printf("print help!\n");
        return 0;
    }
    base = strrchr(argv[1], '/');
    base = base ? base + 1 : argv[1];

    for (a = applets; a->name; ++a)
    {
        if(strcmp(base, a->name) == 0)
        {
            return a->func(argc, argv);
        }
    }
    return 0;
}
