/**************server.c******************/
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include "private_type.h"
#include "private_log.h"
#include "proxy_type.h"
#include "btree/sys_btree.h"
#include "timer.h"

#include "cjson/cJSON.h"
#include "memory/sys_memory.h"

#define PROXYC_TUNNEL_NAME_LEN 64
#define PROXYC_CONNECT_TIMEOUT 60

#define PROXYC_LINK_TUNNEL          0
#define PROXYC_LINK_PROXY_LOCAL     1

typedef struct
{
    UINT8 szTunnelName[PROXYC_TUNNEL_NAME_LEN];
    UINT uiRemotePort;
    UINT uiLocalPort;
    UINT uiLocalIp;
    UINT uiTimer;
    UINT8 ucConnect;
}PROXYC_CFG_TABLE_ST;

typedef struct
{
    PROXYC_CFG_TABLE_ST *pstTables;
    UINT uiTableNum;
    UINT uiTunnelPort;
    UINT uiRemoteIp;
}PROXYC_CFG_ST;
#if 0
#endif
extern PROXYC_CFG_TABLE_ST * PROXYC_GetCfgTableByRemote(IN UINT uiPort);
extern PROXYC_CFG_TABLE_ST * PROXYC_GetCfgTableByLocal(IN UINT uiIp, IN UINT uiPort);
extern int PROXYC_LoopCloseDeal(int uiEpollFd, UINT uiFd);
extern int PROXYC_ClientDelSync(void *timer, void *data);
extern void PROXYC_LinkClientTimeoutExit(void *timer);


#if 0
#endif
static int ReadFile(IN char *szCfgFile, OUT char **ppBuf, UINT *puiLen)
{
    char *szBuf = NULL;
    UINT uiFileLen = 0;
    FILE *fFile = NULL;

    fFile = fopen(szCfgFile, "r");
    if(NULL == fFile)
    {
        CW_SendErr("Cannot open file(%s)",szCfgFile);
        return IPC_STATUS_FAIL;
    }

    fseek(fFile,0,SEEK_END);

    uiFileLen = ftell(fFile);
    szBuf=(char *)MEM_MALLOC(uiFileLen+1);
    if(NULL == szBuf)
    {
        CW_SendErr("Malloc error, siez=%d", uiFileLen+1);
        return IPC_STATUS_MEM_MALLOC_FAIL;
    }

    memset(szBuf, 0, uiFileLen+1);

    rewind(fFile);

    fread(szBuf,1,uiFileLen,fFile);
    szBuf[uiFileLen]=0;
    fclose(fFile);
    *ppBuf = szBuf;
    *puiLen = uiFileLen+1;

    return IPC_STATUS_OK;
}
static int GetCfgTunnelInfoByJson(IN cJSON *pstJson, OUT PROXYC_CFG_TABLE_ST *pstInfo)
{
    cJSON *pstSub = NULL;
    pstSub = cJSON_GetObjectItem(pstJson, "Name");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find Name Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    if (NULL == pstSub->valuestring)
    {
        CW_SendErr("Can not find Name Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    strncpy(pstInfo->szTunnelName, pstSub->valuestring, sizeof(pstInfo->szTunnelName));

    pstSub = cJSON_GetObjectItem(pstJson, "RemotePort");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find RemotePort Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    pstInfo->uiRemotePort = pstSub->valueint;


    pstSub = cJSON_GetObjectItem(pstJson, "LocalPort");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find LocalPort Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    pstInfo->uiLocalPort = pstSub->valueint;

    pstSub = cJSON_GetObjectItem(pstJson, "LocalIp");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find LocalIp Opration!!\n");
        return IPC_STATUS_ARGV;
    }
    if (NULL == pstSub->valuestring)
    {
        CW_SendErr("Can not find LocalIp Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    pstInfo->uiLocalIp= SYS_StrToIp(pstSub->valuestring);
    if(0 == pstInfo->uiLocalIp)
    {
        pstInfo->uiLocalIp = INADDR_ANY;
    }

    return IPC_STATUS_OK;
}
static int GetCfgInfoByJsonSub(IN cJSON *pstJson, OUT PROXYC_CFG_ST *pstCfgInfo)
{
    cJSON *pstSub = NULL;
    UINT i = 0;
    UINT uiUserNum = 0;
    UINT iRet = 0;
    pstSub = cJSON_GetObjectItem(pstJson, "TunnelIp");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find TunnelIp Opration!!\n");
        return IPC_STATUS_ARGV;
    }
    if (NULL == pstSub->valuestring)
    {
        CW_SendErr("Can not find TunnelIp Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    pstCfgInfo->uiRemoteIp = SYS_StrToIp(pstSub->valuestring);
    if(0 == pstCfgInfo->uiRemoteIp)
    {
        CW_SendErr("Tunnel Ip is invalid!!\n");
        return IPC_STATUS_ARGV;
    }
    pstCfgInfo->uiTunnelPort = PROXY_SERVERPORT;

    pstSub = cJSON_GetObjectItem(pstJson, "Tunnels");
	if (NULL == pstSub)
	{
        CW_SendErr("Can not find Opration!!\n");
        return IPC_STATUS_ARGV;
	}
    if(pstSub->type != cJSON_Array)
    {
        CW_SendErr("Type error (%d)!!\n", pstSub->type);
        return IPC_STATUS_ARGV;
	}
    pstCfgInfo->uiTableNum = cJSON_GetArraySize(pstSub);
    pstCfgInfo->pstTables = (PROXYC_CFG_TABLE_ST *)MEM_MALLOC(sizeof(PROXYC_CFG_TABLE_ST) * pstCfgInfo->uiTableNum);
    if(pstCfgInfo->pstTables == NULL)
    {
        CW_SendErr("No memory!!\n");
        return IPC_STATUS_ARGV;
    }
    memset(pstCfgInfo->pstTables, 0, sizeof(PROXYC_CFG_TABLE_ST) * pstCfgInfo->uiTableNum);

    for(i = 0; i < pstCfgInfo->uiTableNum; i++)
    {
        cJSON * pSub = cJSON_GetArrayItem(pstSub, i);
        if(NULL == pSub)
        {
            continue;
        }

        iRet = GetCfgTunnelInfoByJson(pSub, &pstCfgInfo->pstTables[uiUserNum]);
        if (IPC_STATUS_OK == iRet)
        {
            uiUserNum++;
        }
    }
    pstCfgInfo->uiTableNum = uiUserNum;
    return IPC_STATUS_OK;
}
static int GetCfgInfoByJson(IN UINT8 *szBuf, OUT PROXYC_CFG_ST *pstCfgInfo)
{
    cJSON *pstJson = NULL;
    UINT iRet = 0;
    pstJson=cJSON_Parse(szBuf);
    if (!pstJson)
    {
        CW_SendErr("Packet Error: [%s], str=%s\n",cJSON_GetErrorPtr(), szBuf);
        return IPC_STATUS_ARGV;
    }

    iRet = GetCfgInfoByJsonSub(pstJson, pstCfgInfo);
    cJSON_Delete(pstJson);

    return iRet;
}
int PROXYC_GetCfgInfo(IN char *szCfgFile, OUT PROXYC_CFG_ST *pstCfgInfo)
{
    char *szBuf = NULL;
    UINT uiLen = 0;
    UINT uiRet = IPC_STATUS_OK;

    if((szCfgFile == NULL) || (pstCfgInfo == NULL))
    {
        CW_SendErr("Point is NULL");
        return IPC_STATUS_FAIL;
    }

    uiRet = ReadFile(szCfgFile, &szBuf, &uiLen);
    if(IPC_STATUS_OK != uiRet)
    {
        CW_SendErr("Get info from file error!! file=%s",szCfgFile);
        return uiRet;
    }

    if(NULL == szBuf)
    {
        CW_SendErr("Malloc error!! file=%s",szCfgFile);
        return IPC_STATUS_MEM_MALLOC_FAIL;
    }

    uiRet =GetCfgInfoByJson(szBuf, pstCfgInfo);
    MEM_FREE(szBuf);
    if(IPC_STATUS_OK != uiRet)
    {
        CW_SendErr("Get info from json error!! cfg=%s",szBuf);
        return uiRet;
    }

    return IPC_STATUS_OK;
}

int PROXYC_PrintCfgInfo(IN PROXYC_CFG_ST *pstCfgInfo)
{
    char *szBuf = NULL;
    UINT uiLen = 0;
    UINT uiRet = IPC_STATUS_OK;
    UINT i = 0;
    PROXYC_CFG_TABLE_ST *pstTables = NULL;

    if((pstCfgInfo == NULL))
    {
        CW_SendErr("Point is NULL");
        return IPC_STATUS_FAIL;
    }
    CW_SendDebug("uiRemoteIp = %s(%d)",
               inet_ntoa(*(struct in_addr*)&(pstCfgInfo->uiRemoteIp)),
               pstCfgInfo->uiRemoteIp);
    CW_SendDebug("uiTunnelPort = (%d), tunnelnum=%d",
               pstCfgInfo->uiTunnelPort, pstCfgInfo->uiTableNum);
    for(i = 0; i< pstCfgInfo->uiTableNum; i++)
    {
        pstTables = &pstCfgInfo->pstTables[i];

        CW_SendDebug("szTunnelName = (%s)",
                   pstTables->szTunnelName);
        CW_SendDebug("uiRemotePort = (%d)",
                   pstTables->uiRemotePort);
        CW_SendDebug("uiLocalPort = (%d)",
                   pstTables->uiLocalPort);
        CW_SendDebug("uiRemoteIp = %s(%d)",
                   inet_ntoa(*(struct in_addr*)&(pstTables->uiLocalIp)),
                   pstTables->uiLocalIp);
    }


    return IPC_STATUS_OK;
}

#if 0
#endif

typedef struct
{
    UINT uiSocketFd;
    UINT uiIp;
    UINT16 uiPort;
}PROXYC_TOPO_INDEX_T;

typedef struct
{
    PROXYC_TOPO_INDEX_T stTunnelIndex;
    UINT uiRemoteSocketFd;
    UINT uiRemoteIp;
    UINT16 usRemotePort;
    UINT16 uiRemoteServerPort;
}PROXYC_TOPO_LOCAL_DATA_T;

typedef struct
{
    PROXYC_TOPO_INDEX_T stIndex;
    UINT8 ucLinkType; /* PROXYC_LINK_PROXY_LOCAL */
    union
    {
        PROXYC_TOPO_LOCAL_DATA_T stLocal;   /* for PROXYC_LINK_PROXY_LOCAL */
    } unDevice;
    PROXY_PACKET_BUFF_T stPktBuf;
}PROXYC_TOPO_INFO_T;

typedef struct
{
    int uiEpollFd;
    PROXYC_TOPO_INFO_T stTopo;
}PROXYC_TOPO_TIMER_T;


typedef struct ProxyCTreeNode_t
{
    PROXYC_TOPO_INFO_T *pData;
    struct ProxyCTreeNode_t *Child[BI_TREE_CHILD_NUM];
}BT_PROXYC_NODE_ST;

BT_PROXYC_NODE_ST g_stProxycSocketHead;  /* socket id 作为索引 */
BT_PROXYC_NODE_ST g_stProxycIpHead;  /* ip和端口 作为索引 */

#if 0
#endif
PROXYC_TOPO_INFO_T *PROXYC_FindProxyTopoByFd(UINT uiFd)
{
    BT_INDEX_ST stIndex;
    BT_PROXYC_NODE_ST *pstNode = NULL;

    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = uiFd;
    stIndex.uiIndexLen = 32;
    pstNode = (BT_PROXYC_NODE_ST *)BT_FindBTree((BT_NODE_ST *)&g_stProxycSocketHead, &stIndex);
    if(pstNode!= NULL)
    {
        return pstNode->pData;
    }

    return NULL;
}

PROXYC_TOPO_INFO_T *PROXYC_FindProxyTopoByIp(UINT uiIp, UINT16 usPort)
{
    BT_INDEX_ST stIndex;
    BT_PROXYC_NODE_ST *pstNode = NULL;

    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = uiIp;
    stIndex.auiIndex[1] = usPort;
    stIndex.uiIndexLen = 64;
    pstNode = (BT_PROXYC_NODE_ST *)BT_FindBTree((BT_NODE_ST *)&g_stProxycIpHead, &stIndex);
    if(pstNode!= NULL)
    {
        return pstNode->pData;
    }

    return NULL;
}

int PROXYC_AddProxyTopo(PROXYC_TOPO_INFO_T *pstData)
{
    BT_INDEX_ST stIndex;
    BT_PROXYC_NODE_ST *pstNode = NULL;
    PROXYC_TOPO_INFO_T *pstAlloc = NULL;

    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiSocketFd;
    stIndex.uiIndexLen = 32;
    pstNode = (BT_PROXYC_NODE_ST *)BT_AddBTree((BT_NODE_ST *)&g_stProxycSocketHead, &stIndex);
    if(pstNode!= NULL)
    {
        pstNode->pData = (PROXYC_TOPO_INFO_T *)MEM_MALLOC(sizeof(PROXYC_TOPO_INFO_T));
        memcpy(pstNode->pData, pstData, sizeof(PROXYC_TOPO_INFO_T));
        pstAlloc = pstNode->pData;
    }

    /* 按照ip创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiIp;
    stIndex.auiIndex[1] = pstData->stIndex.uiPort;
    stIndex.uiIndexLen = 64;
    pstNode = (BT_PROXYC_NODE_ST *)BT_AddBTree((BT_NODE_ST *)&g_stProxycIpHead, &stIndex);
    if(pstNode!= NULL)
    {
        if(NULL == pstAlloc)
        {
            pstNode->pData = (PROXYC_TOPO_INFO_T *)MEM_MALLOC(sizeof(PROXYC_TOPO_INFO_T));
            memcpy(pstNode->pData, pstData, sizeof(PROXYC_TOPO_INFO_T));
            pstAlloc = pstNode->pData;
        }
        else
        {
            pstNode->pData = pstAlloc;
        }
    }

    CW_SendInfo( "ip=%s, port=%d fd=%d",
                  inet_ntoa(*(struct in_addr*)&(pstData->stIndex.uiIp)),
                  pstData->stIndex.uiPort, pstData->stIndex.uiSocketFd);
    return IPC_STATUS_OK;
}
static int ProxycTopoDelFuncCommon(void *pInData, BT_NODE_ST *pstNode)
{
    if((NULL != pstNode) && (NULL != pstNode->pData))
    {
        /* 由于两处地方都用了这一块内存，所以统一释放 */
        pstNode->pData = NULL;
    }
    return TRUE;
}

int PROXYC_DelProxyTopo(PROXYC_TOPO_INFO_T *pstData)
{
    BT_INDEX_ST stIndex;
    BT_PROXYC_NODE_ST *pstNode = NULL;
    PROXYC_TOPO_INFO_T *pstAlloc = NULL;
    PROXYC_TOPO_TIMER_T *pstTimer=NULL;

    pstAlloc = PROXYC_FindProxyTopoByFd(pstData->stIndex.uiSocketFd);
    if(NULL == pstAlloc)
    {
        return IPC_STATUS_OK;
    }
    CW_SendInfo( "ip=%s, port=%d fd=%d",
                  inet_ntoa(*(struct in_addr*)&(pstData->stIndex.uiIp)),
                  pstData->stIndex.uiPort, pstData->stIndex.uiSocketFd);
    /* 按照socket创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiSocketFd;
    stIndex.uiIndexLen = 32;
    BT_DelTree((BT_NODE_ST *)&g_stProxycSocketHead, &stIndex, NULL,ProxycTopoDelFuncCommon);

    /* 按照ip创建表 */
    memset(&stIndex, 0, sizeof(stIndex));
    stIndex.auiIndex[0] = pstData->stIndex.uiIp;
    stIndex.auiIndex[1] = pstData->stIndex.uiPort;
    stIndex.uiIndexLen = 64;
    BT_DelTree((BT_NODE_ST *)&g_stProxycIpHead, &stIndex, NULL,ProxycTopoDelFuncCommon);

    switch(pstAlloc->ucLinkType)
    {
        case PROXYC_LINK_TUNNEL:
        case PROXYC_LINK_PROXY_LOCAL:
            pstTimer = (PROXYC_TOPO_TIMER_T *)MEM_MALLOC(sizeof(PROXYC_TOPO_TIMER_T));
            if(NULL == pstTimer)
            {
                CW_SendErr( "malloc error! size=%d", sizeof(PROXYC_TOPO_TIMER_T));
                return IPC_STATUS_MEM_MALLOC_FAIL;
            }
            pstTimer->uiEpollFd = 0;
            memcpy(&pstTimer->stTopo, pstAlloc, sizeof(pstTimer->stTopo));
            timer_register(PROXY_SYNC_TIMEOUT*1000, 0, (timer_func_t)PROXYC_ClientDelSync , pstTimer, PROXYC_LinkClientTimeoutExit, "tunnel_client_sync_timeout");
            break;
         default:
            break;
    }


    if(NULL != pstAlloc->stPktBuf.s_LastPktBuf)
    {
        MEM_FREE(pstAlloc->stPktBuf.s_LastPktBuf);
    }
    MEM_FREE(pstAlloc);
    return IPC_STATUS_OK;
}

static int ProxycTopoFreeFuncCommon(void *pInData, BT_NODE_ST *pstNode)
{
    PROXYC_TOPO_INFO_T *pstAlloc = NULL;
    if((NULL != pstNode) && (NULL != pstNode->pData))
    {
        pstAlloc = pstNode->pData;
        pstNode->pData = NULL;

        if(NULL != pstAlloc->stPktBuf.s_LastPktBuf)
        {
            MEM_FREE(pstAlloc->stPktBuf.s_LastPktBuf);
        }
        close(pstAlloc->stIndex.uiSocketFd);
        CW_SendInfo( "ip=%s, port=%d fd=%d",
                      inet_ntoa(*(struct in_addr*)&(pstAlloc->stIndex.uiIp)),
                      pstAlloc->stIndex.uiPort, pstAlloc->stIndex.uiSocketFd);

        MEM_FREE(pstAlloc);
    }
    return TRUE;
}

int PROXYC_FreeProxyTopo()
{

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxycSocketHead, NULL,
                        BT_GetAllBTreeFuncCheckNone, ProxycTopoFreeFuncCommon);

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxycIpHead, NULL,
                        BT_GetAllBTreeFuncCheckNone, ProxycTopoDelFuncCommon);

    return IPC_STATUS_OK;
}
#if 0
#endif
void PROXYC_LinkClientTimeoutExit(void *timer)
{
    timer_element_t *tmp = timer;
    MEM_FREE(tmp->data);
    return;
}
int PROXYC_FreeProxyTopoByTunnelCheck(void *TreeData, void *CheckData)
{
    PROXYC_TOPO_INFO_T *pstCheck = CheckData;
    PROXYC_TOPO_INFO_T *pstData = CheckData;

    if((NULL == pstCheck) || (NULL == pstData))
    {
        CW_SendErr( "Point is NULL!");
        return FALSE;
    }

    switch(pstData->ucLinkType)
    {
        case PROXYC_LINK_PROXY_LOCAL:
            if(0 != memcpy(&pstData->unDevice.stLocal.stTunnelIndex,
                            &pstCheck->stIndex, sizeof(pstCheck->stIndex)))
            {
                return FALSE;
            }
            break;
         default:
            break;

    }

    return TRUE;
}

int PROXYC_FreeProxyTopoByTunnel(PROXYC_TOPO_INFO_T *pstTopo)
{

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxycSocketHead, pstTopo,
                        PROXYC_FreeProxyTopoByTunnelCheck, ProxycTopoFreeFuncCommon);

    BT_DelBTreeByCheck((BT_NODE_ST *)&g_stProxycIpHead, pstTopo,
                        PROXYC_FreeProxyTopoByTunnelCheck, ProxycTopoDelFuncCommon);

    return IPC_STATUS_OK;
}

int PROXYC_ClientDelSync(void *timer, void *data)
{
    PROXYC_TOPO_TIMER_T *pstTimer = data;
    switch(pstTimer->stTopo.ucLinkType)
    {
        case PROXYC_LINK_TUNNEL:
            PROXYC_FreeProxyTopoByTunnel(&pstTimer->stTopo);
            break;
        case PROXYC_LINK_PROXY_LOCAL:
            PROXYC_FreeProxyTopoByProxyClient(&pstTimer->stTopo);
            break;
         default:
            break;

    }
    return TIMER_RUN_ONCE;
}

int PROXYC_FreeProxyTopoByProxyClient(PROXYC_TOPO_INFO_T *pstTopo)
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN] = {0};
    PROXY_PACKET_HEAD_T *pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    INT8 *pstManage = aucPktBuf + sizeof(PROXY_PACKET_HEAD_T);
    cJSON *pstJson = NULL;
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_MANAGE;

    pstJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(pstJson, "ManageType",PROXY_PKT_TYPE_RELEASE);

    cJSON_AddNumberToObject(pstJson, "Status",0);
    cJSON_AddNumberToObject(pstJson, "LinkClientFd",pstTopo->unDevice.stLocal.uiRemoteSocketFd);
    cJSON_AddNumberToObject(pstJson, "LinkClientIp",pstTopo->unDevice.stLocal.uiRemoteIp);
    cJSON_AddNumberToObject(pstJson, "LinkClientPort",pstTopo->unDevice.stLocal.usRemotePort);
    cJSON_AddNumberToObject(pstJson, "LinkServerPort",
                            pstTopo->unDevice.stLocal.uiRemoteServerPort);
    cJSON_AddNumberToObject(pstJson, "ProxydFd",pstTopo->stIndex.uiSocketFd);

    char *szOutJson = cJSON_Print(pstJson);

    strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));

    pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));

    write(pstTopo->unDevice.stLocal.stTunnelIndex.uiSocketFd,
         aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
    free(szOutJson);
    cJSON_Delete(pstJson);

    return IPC_STATUS_OK;
}

#if 0
#endif

void PROXYC_SocketSendTunnelCfg(IN PROXYC_TOPO_INFO_T *pstTunnel, IN PROXYC_CFG_TABLE_ST *pstTables)
{
    INT8 aucPktBuf[PROXY_SOCK_MAX_PKT_LEN] = {0};
    PROXY_PACKET_HEAD_T *pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    INT8 *pstManage = aucPktBuf + sizeof(PROXY_PACKET_HEAD_T);
    cJSON *pstJson = NULL;
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_MANAGE;

    pstJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(pstJson, "ManageType",PROXY_PKT_TYPE_TUNNEL_CFG);

    cJSON_AddNumberToObject(pstJson, "Status", 0);

    cJSON_AddNumberToObject(pstJson, "LinkServerPort",pstTables->uiRemotePort);

    cJSON_AddNumberToObject(pstJson, "ProxyfPort",pstTables->uiLocalPort);
    cJSON_AddNumberToObject(pstJson, "ProxyfIp",pstTables->uiLocalIp);

    char *szOutJson = cJSON_Print(pstJson);

    strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));
    CW_SendDebug("connect cfg start : %s\n", szOutJson);

    pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
    write(pstTunnel->stIndex.uiSocketFd,
         aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
    free(szOutJson);
    cJSON_Delete(pstJson);

    return ;
}
int  PROXYC_SocketLinkAckPktDeal
(
    IN int uiEpollFd,
    IN PROXYC_TOPO_INFO_T *pstTunnelInfo,
    INOUT PROXY_PACKET_LINK_REQUEST_T *pstInfo
)
{
    PROXYC_TOPO_INFO_T stTopo;
    PROXYC_CFG_TABLE_ST * pstCfgTable = NULL;
    int iFd = -1;
    struct sockaddr_in stGuest;
    int iGuestLen = sizeof(stGuest);
    char szGuestIp[20];

    pstCfgTable = PROXYC_GetCfgTableByRemote(pstInfo->uiLinkServerPort);
    if(NULL == pstCfgTable)
    {
        return IPC_STATUS_NOT_FIND;
    }

    iFd = SOCKET_TcpClientConnect(pstCfgTable->uiLocalIp, pstCfgTable->uiLocalPort);
    if(iFd < 0)
    {
        CW_SendErr("server not exist!!ip=%d, port=%d", pstCfgTable->uiLocalIp, pstCfgTable->uiLocalPort);
        return IPC_STATUS_BUSY;
    }
    getsockname(iFd, (struct sockaddr *)&stGuest, &iGuestLen);

    inet_ntop(AF_INET, &stGuest.sin_addr, szGuestIp, sizeof(szGuestIp));

    memset(&stTopo, 0, sizeof(stTopo));
    stTopo.stIndex.uiSocketFd= iFd;
    stTopo.stIndex.uiPort = ntohs(stGuest.sin_port);
    stTopo.stIndex.uiIp = stGuest.sin_addr.s_addr;
    stTopo.ucLinkType = PROXYC_LINK_PROXY_LOCAL;
    stTopo.unDevice.stLocal.uiRemoteSocketFd = pstInfo->uiLinkClientFd;
    stTopo.unDevice.stLocal.uiRemoteIp = pstInfo->uiLinkClientIp;
    stTopo.unDevice.stLocal.usRemotePort = pstInfo->uiLinkClientPort;
    stTopo.unDevice.stLocal.uiRemoteServerPort = pstInfo->uiLinkServerPort;
    memcpy(&stTopo.unDevice.stLocal.stTunnelIndex, &pstTunnelInfo->stIndex, sizeof(pstTunnelInfo->stIndex));
    PROXYC_AddProxyTopo(&stTopo);
    CW_SendDebug( "remoteip=%s, remote port=%d fd=%d",
                  inet_ntoa(*(struct in_addr*)&(stTopo.unDevice.stLocal.uiRemoteIp)),
                  stTopo.unDevice.stLocal.usRemotePort, stTopo.unDevice.stLocal.uiRemoteSocketFd);

    SOCKET_AddEpoll(uiEpollFd, iFd, EPOLLIN);
    pstInfo->uiProxydFd = iFd;

    return IPC_STATUS_OK;
}

int  PROXYC_SocketTunnelPktDeal
(
    IN int uiEpollFd,
    IN PROXYC_TOPO_INFO_T *pstTopoInfo,
    INOUT UINT8 *aucPktBuf,
    IN UINT uiPktLen
)
{
    UINT8 *pstManage = NULL;
    PROXYC_TOPO_INFO_T stNewServer;
    cJSON *pstJson = NULL;
    cJSON *pstSub = NULL;
    UINT uiRet = 0;
    PROXY_PACKET_MIRROR_T stMirrorInfo;
    char *szOutJson = NULL;
    PROXY_PACKET_LINK_REQUEST_T stLinkReqInfo;
    PROXYC_CFG_TABLE_ST * pstCfgTable = NULL;
    PROXY_PACKET_HEAD_T *pstHead = NULL;

    if (uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        CW_SendErr("ManagePkt is too short!! %d\n",
                      uiPktLen);
        return IPC_STATUS_FAIL;
    }
    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstManage = aucPktBuf+sizeof(PROXY_PACKET_HEAD_T);

    CW_SendDebug("rev=%s", pstManage);

    pstJson=cJSON_Parse(pstManage);
    if (!pstJson)
    {
        CW_SendErr("Packet Error: [%s], str=%s\n",cJSON_GetErrorPtr(), pstManage);
        return IPC_STATUS_ARGV;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ManageType");
    if (NULL == pstSub)
    {
        CW_SendErr("Can not find Opration!!\n");
        return IPC_STATUS_ARGV;
    }

    switch(pstSub->valueint)
    {
        case PROXY_PKT_TYPE_CONNECT:
            memset(&stLinkReqInfo, 0, sizeof(stLinkReqInfo));
            PROXY_SocketGetLinkAckInfo(pstJson, &stLinkReqInfo);
            uiRet = PROXYC_SocketLinkAckPktDeal(uiEpollFd,pstTopoInfo,&stLinkReqInfo);

            cJSON_SetIntValue(pstSub, PROXY_PKT_TYPE_CONNECT_ACK);

            pstSub = cJSON_GetObjectItem(pstJson, "Status");
            if (NULL == pstSub)
            {
                CW_SendErr("Can not find Status!!\n");
                return IPC_STATUS_ARGV;
            }
            cJSON_SetIntValue(pstSub, uiRet);

            pstSub = cJSON_GetObjectItem(pstJson, "ProxydFd");
            if (NULL == pstSub)
            {
                CW_SendErr("Can not find ProxydFd!!\n");
                return IPC_STATUS_ARGV;
            }
            cJSON_SetIntValue(pstSub, stLinkReqInfo.uiProxydFd);

            szOutJson = cJSON_Print(pstJson);
            strncpy(pstManage, szOutJson, PROXY_SOCK_MAX_PKT_LEN-sizeof(PROXY_PACKET_HEAD_T));

            pstHead->uiPktLen = htonl(sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));

            write(pstTopoInfo->stIndex.uiSocketFd, aucPktBuf, sizeof(PROXY_PACKET_HEAD_T)+strlen(szOutJson));
            free(szOutJson);
            break;
        case PROXY_PKT_TYPE_TUNNEL_CFG_ACK:
            memset(&stMirrorInfo, 0, sizeof(stMirrorInfo));
            PROXY_SocketGetLinkServerInfo(pstJson, &stMirrorInfo);

            pstCfgTable = PROXYC_GetCfgTableByLocal(stMirrorInfo.uiProxyfIp, stMirrorInfo.uiProxyfPort);
            if(NULL == pstCfgTable)
            {
                CW_SendErr("Cannot find cfgtable, ip=%x, port=%d", stMirrorInfo.uiProxyfIp, stMirrorInfo.uiProxyfPort);
                break;
            }
            if(pstCfgTable->uiTimer >= 0)
            {
                timer_cancel(pstCfgTable->uiTimer);
            }
            pstCfgTable->uiTimer = -1;

            if((IPC_STATUS_OK == stMirrorInfo.usStatus) ||
               (IPC_STATUS_EXIST == stMirrorInfo.usStatus))
            {
                if(FALSE == pstCfgTable->ucConnect)
                {
                    CW_SendErr("Cfg connect, name=%s", pstCfgTable->szTunnelName);
                }
                pstCfgTable->ucConnect = TRUE;
            }
            else
            {
                if(TRUE == pstCfgTable->ucConnect)
                {
                    CW_SendErr("Cfg disconnect, name=%s", pstCfgTable->szTunnelName);
                }
                pstCfgTable->ucConnect = FALSE;
            }

            break;
        case PROXY_PKT_TYPE_RELEASE:
            memset(&stLinkReqInfo, 0, sizeof(stLinkReqInfo));
            PROXY_SocektGetReleaseInfo(pstJson, &stLinkReqInfo);
            PROXYC_TOPO_INFO_T *pstAlloc = NULL;
            pstAlloc = PROXYC_FindProxyTopoByFd(stLinkReqInfo.uiProxydFd);
            if(pstAlloc != NULL)
            {
                PROXYC_DelProxyTopo(pstAlloc);
                close(stLinkReqInfo.uiProxydFd);
            }
            break;
        default:
            break;
    }

    cJSON_Delete(pstJson);

    return IPC_STATUS_OK;

}
int  PROXYC_SocketTunnelDataPktDeal
(
    PROXYC_TOPO_INFO_T *pstTopoInfo,
    UINT8 *aucPktBuf,
    UINT uiPktLen
)
{
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    PROXY_PACKET_DATA_CTL_T *pstDataCtl = NULL;
    UINT uiHeadLen = sizeof(PROXY_PACKET_HEAD_T) + sizeof(PROXY_PACKET_DATA_CTL_T);
    INT8 *aucPktData = aucPktBuf + uiHeadLen;
    UINT uiDFd = -1;
    PROXYC_TOPO_INFO_T *pstTopo = NULL;

    CW_SendDebug("Read data from %d:ip=%s:%d port=%d\n",
              pstTopoInfo->stIndex.uiSocketFd,
              inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
               pstTopoInfo->stIndex.uiIp,
              (pstTopoInfo->stIndex.uiPort));
    PKT_Dump(aucPktBuf, uiPktLen);

    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstDataCtl = (PROXY_PACKET_DATA_CTL_T *)(aucPktBuf+sizeof(PROXY_PACKET_HEAD_T));
    uiDFd = ntohl(pstDataCtl->uiProxyFd);

    pstTopo = PROXYC_FindProxyTopoByFd(uiDFd);
    if(NULL == pstTopo)
    {
        CW_SendErr("Can not find topo, fd=%d!!\n", uiDFd);
        return IPC_STATUS_NOT_FIND;
    }
    else
    {

        CW_SendDebug("send data to %d:ip=%s:%d, port=%d\n",
                  pstTopo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopo->stIndex.uiIp)),
                  pstTopo->stIndex.uiIp,
                  (pstTopo->stIndex.uiPort));
    }
    PKT_Dump(aucPktData, uiPktLen-uiHeadLen);

    write(uiDFd, aucPktData, uiPktLen-uiHeadLen);

    return IPC_STATUS_OK;
}

void PROXYC_SocketTunnelReadPkt
(
    int uiEpollFd,
    PROXYC_TOPO_INFO_T *pstTopoInfo,
    INT8 *aucPktBuf,
    int uiPktLen
)
{
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    if (uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        return;
    }
    CW_SendDebug("Read from server %d:ip=%s:%d port=%d\n",
                  pstTopoInfo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
                  pstTopoInfo->stIndex.uiIp,
                  (pstTopoInfo->stIndex.uiPort));

    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    if(0 != memcmp(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic)))
    {
        return;
    }

    switch(pstHead->ucOpration)
    {
        case PROXY_PACKET_OP_MANAGE:
            PROXYC_SocketTunnelPktDeal(uiEpollFd, pstTopoInfo,
                                      aucPktBuf,
                                      uiPktLen);
            break;
        case PROXY_PACKET_OP_DATA:
            PROXYC_SocketTunnelDataPktDeal(pstTopoInfo,aucPktBuf,uiPktLen);
            break;
        default:
            break;
    }

    return;
}

#if 0
void PROXYC_SocketTunnelRead(int uiEpollFd, PROXYC_TOPO_INFO_T *pstTopoInfo)
{
    static INT8 *s_LastPktBuf = NULL;
    static int s_uiLastPktLen = 0;
    INT8 aucBuf[PROXY_SOCK_MAX_PKT_LEN];
    int uiLen = 0;
    INT8 *pucPktBuf = NULL;
    int uiPktLen = 0;
    int uiHeadOffset = 0;

    uiLen = read(pstTopoInfo->stIndex.uiSocketFd, aucBuf, PROXY_SOCK_MAX_PKT_LEN);
    if(NULL == s_LastPktBuf)
    {
        s_uiLastPktLen = 0;
    }

    /* 将上一次未处理的报文与本次收到的报文合并 */
    pucPktBuf = MEM_MALLOC(uiLen+s_uiLastPktLen);
    if(NULL == pucPktBuf)
    {
        CW_SendErr("malloc size(%d) error!!", uiLen+s_uiLastPktLen);
        return;
    }
    memset(pucPktBuf, 0, uiLen+s_uiLastPktLen);
    if(NULL != s_LastPktBuf)
    {
        memcpy(pucPktBuf, s_LastPktBuf, s_uiLastPktLen);
        uiPktLen = s_uiLastPktLen;

        MEM_FREE(s_LastPktBuf);
        s_LastPktBuf = NULL;
        s_uiLastPktLen = 0;
    }

    memcpy(&pucPktBuf[uiPktLen], aucBuf, uiLen);
    uiPktLen += uiLen;

    /* 报文过短不处理 */
    if(uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        s_LastPktBuf = pucPktBuf;
        s_uiLastPktLen = uiPktLen;
        return;
    }

    uiHeadOffset = PROXY_PktSeekHead(pucPktBuf, uiPktLen);
    if(uiHeadOffset == uiPktLen)
    {
        s_LastPktBuf = pucPktBuf;
        s_uiLastPktLen = uiPktLen;
        return;
    }

    while(1)
    {
        INT8 *pucPktBufTmp = &pucPktBuf[uiHeadOffset];
        int uiPktLenTmp = uiPktLen - uiHeadOffset;
        int uiUsedLen = 0;
        int ret = 0;

        ret = PROXY_GetOnePkt(pucPktBufTmp, uiPktLenTmp, &uiUsedLen);
        if(IPC_STATUS_OK != ret)
        {
            s_LastPktBuf = MEM_MALLOC(uiPktLenTmp);
            memcpy(s_LastPktBuf, pucPktBufTmp, uiPktLenTmp);
            s_uiLastPktLen = uiPktLenTmp;

            MEM_FREE(pucPktBuf);
            return;
        }

        PROXYC_SocketTunnelReadPkt(uiEpollFd, pstTopoInfo, pucPktBufTmp,uiUsedLen);

        uiHeadOffset += uiUsedLen;

        if(uiHeadOffset >= uiPktLen)
        {
            break;
        }
    }
    MEM_FREE(pucPktBuf);

    return;
}
#endif
#if 0
#endif
void PROXYC_SocketLinkRead(int uiEpollFd, PROXYC_TOPO_INFO_T *pstTopoInfo)
{
    INT8 aucPktBuf[PROXY_CLIENT_SOCK_MAX_PKT_LEN];
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    PROXY_PACKET_DATA_CTL_T *pstDataCtl = NULL;
    UINT uiHeadLen = sizeof(PROXY_PACKET_HEAD_T) + sizeof(PROXY_PACKET_DATA_CTL_T);
    INT8 *aucPktData = aucPktBuf + uiHeadLen;

    int uiPktLen = read(pstTopoInfo->stIndex.uiSocketFd, aucPktData, PROXY_CLIENT_SOCK_MAX_PKT_LEN-uiHeadLen);
    if(uiPktLen == 0)
    {
        SOCKET_DelEpoll(uiEpollFd, pstTopoInfo->stIndex.uiSocketFd, PROXYC_LoopCloseDeal);
        return;
    }
    CW_SendDebug("Read from server %d:ip=%s:%d, port=%d, len=%d\n",
                  pstTopoInfo->stIndex.uiSocketFd,
                  inet_ntoa(*(struct in_addr*)&(pstTopoInfo->stIndex.uiIp)),
                  pstTopoInfo->stIndex.uiIp,
                  (pstTopoInfo->stIndex.uiPort), uiPktLen);

    pstHead = (PROXY_PACKET_HEAD_T *)aucPktBuf;
    pstDataCtl = (PROXY_PACKET_DATA_CTL_T *)(aucPktBuf+sizeof(PROXY_PACKET_HEAD_T));
    memcpy(pstHead->aucMagic, g_szMagic, sizeof(g_szMagic));
    pstHead->ucOpration = PROXY_PACKET_OP_DATA;
    pstDataCtl->uiLinkFd = htonl(pstTopoInfo->unDevice.stLocal.uiRemoteSocketFd);
    pstDataCtl->uiProxyFd = htonl(pstTopoInfo->stIndex.uiSocketFd);

    pstHead->uiPktLen = htonl(uiPktLen+uiHeadLen);

    PKT_Dump(aucPktBuf, uiPktLen+uiHeadLen);
    CW_SendDump("\n");

    write(pstTopoInfo->unDevice.stLocal.stTunnelIndex.uiSocketFd,
         aucPktBuf, uiPktLen+uiHeadLen);
    return;
}

#if 0
#endif
int PROXYC_LoopReadDeal(int uiEpollFd, UINT uiFd)
{
    PROXYC_TOPO_INFO_T *pstTopoInfo = NULL;
    pstTopoInfo = PROXYC_FindProxyTopoByFd(uiFd);
    if(NULL == pstTopoInfo)
    {
        CW_SendDebug("WARNING: NOT table value for event id() and sock(%d)", uiFd);
        return IPC_STATUS_FAIL;
    }
    switch(pstTopoInfo->ucLinkType)
    {
        case PROXYC_LINK_TUNNEL:
            PROXY_SocketRead(uiEpollFd, pstTopoInfo->stIndex.uiSocketFd,
                            pstTopoInfo, &pstTopoInfo->stPktBuf,PROXYC_SocketTunnelReadPkt,
                            PROXYC_LoopCloseDeal);
            //PROXYC_SocketTunnelRead(uiEpollFd, pstTopoInfo);
            break;
        case PROXYC_LINK_PROXY_LOCAL:
            PROXYC_SocketLinkRead(uiEpollFd, pstTopoInfo);
            break;
        default:
            CW_SendErr("Link type error! type=%d sock=%d", pstTopoInfo->ucLinkType, uiFd);
            break;
    }
    return IPC_STATUS_OK;
}

int PROXYC_LoopCloseDeal(int uiEpollFd, UINT uiFd)
{
    PROXYC_TOPO_INFO_T *pstTopoInfo = NULL;
    pstTopoInfo = PROXYC_FindProxyTopoByFd(uiFd);
    if(NULL == pstTopoInfo)
    {
        CW_SendDebug("WARNING: NOT table value for event id() and sock(%d)", uiFd);
        return IPC_STATUS_FAIL;
    }
    PROXYC_DelProxyTopo(pstTopoInfo);
}

#if 0
#endif
PROXYC_CFG_ST g_stCfgInfo;
PROXYC_CFG_TABLE_ST * PROXYC_GetCfgTableByLocal(IN UINT uiIp, IN UINT uiPort)
{
    int i = 0;
    for(i = 0; i < g_stCfgInfo.uiTableNum; i++)
    {
        if((g_stCfgInfo.pstTables[i].uiLocalIp == uiIp) &&
           (g_stCfgInfo.pstTables[i].uiLocalPort == uiPort))
        {
            return &g_stCfgInfo.pstTables[i];
        }
    }
    return NULL;
}
PROXYC_CFG_TABLE_ST * PROXYC_GetCfgTableByRemote(IN UINT uiPort)
{
    int i = 0;

    for(i = 0; i < g_stCfgInfo.uiTableNum; i++)
    {
        if((g_stCfgInfo.pstTables[i].uiRemotePort == uiPort))
        {
            return &g_stCfgInfo.pstTables[i];
        }
    }
    return NULL;
}

#if 0
#endif
int PROXYC_CfgInit()
{
    memset(&g_stProxycIpHead, 0, sizeof(g_stProxycIpHead));
    memset(&g_stProxycSocketHead, 0, sizeof(g_stProxycSocketHead));
    memset(&g_stCfgInfo, 0, sizeof(g_stCfgInfo));
}

int PROXYC_CfgSendTimeout(void *timer, void *data)
{
    PROXYC_CFG_TABLE_ST *pstTable = data;

    pstTable->uiTimer = -1;
    if(TRUE == pstTable->ucConnect)
    {
        CW_SendErr("Cfg disconnect, name=%s", pstTable->szTunnelName);
    }
    pstTable->ucConnect = FALSE;

    return TIMER_RUN_ONCE;
}

int PROXYC_CfgTimeout(void *timer, void *data)
{
    UINT *puiIndex = data;
    PROXYC_TOPO_INFO_T *pstTopo = NULL;
    UINT uiIndex = *puiIndex;

    MEM_FREE(puiIndex);

    puiIndex = NULL;
    pstTopo = PROXYC_FindProxyTopoByIp(g_stCfgInfo.uiRemoteIp, g_stCfgInfo.uiTunnelPort);
    if(NULL == pstTopo)
    {
        CW_SendErr("Cannot find tunnel: ip=%d, port=%d", g_stCfgInfo.uiRemoteIp, g_stCfgInfo.uiTunnelPort);
        return TIMER_RUN_ONCE;
    }

    if(uiIndex >= g_stCfgInfo.uiTableNum)
    {
        CW_SendErr("Tunnel table error: index=%d", uiIndex);
        return TIMER_RUN_ONCE;
    }

    PROXYC_SocketSendTunnelCfg(pstTopo, &(g_stCfgInfo.pstTables[uiIndex]));

    g_stCfgInfo.pstTables[uiIndex].uiTimer = timer_register(PROXY_LINK_CLIENT_TIMEOUT*1000, FALSE,
                                                (timer_func_t)PROXYC_CfgSendTimeout,
                                                &(g_stCfgInfo.pstTables[uiIndex]),
                                                NULL, "proxyc_cfg_request");

    return TIMER_RUN_ONCE;
}
int PROXYC_ConnectTimeout(void *timer, void *data)
{
    int *puiEpollFd = data;
    int uiFd = 0;
    PROXYC_TOPO_INFO_T *pstTopo = NULL;
    PROXYC_TOPO_INFO_T stTmp;
    UINT i =0;
    UINT *puiIndex = NULL;

    pstTopo = PROXYC_FindProxyTopoByIp(g_stCfgInfo.uiRemoteIp, g_stCfgInfo.uiTunnelPort);
    if(NULL == pstTopo)
    {
        uiFd = SOCKET_TcpClientConnect(g_stCfgInfo.uiRemoteIp, g_stCfgInfo.uiTunnelPort);
        if(uiFd < 0)
        {
            return TIMER_RUN_FOREVER;
        }
        CW_SendInfo("connect tunnel: %s:%d \n", inet_ntoa(*(struct in_addr*)&(g_stCfgInfo.uiRemoteIp)),g_stCfgInfo.uiTunnelPort);

        memset(&stTmp, 0, sizeof(stTmp));
        stTmp.stIndex.uiSocketFd= uiFd;
        stTmp.stIndex.uiPort = g_stCfgInfo.uiTunnelPort;
        stTmp.stIndex.uiIp = g_stCfgInfo.uiRemoteIp;
        stTmp.ucLinkType = PROXYC_LINK_TUNNEL;
        PROXYC_AddProxyTopo(&stTmp);

        SOCKET_AddEpoll(*puiEpollFd, uiFd, EPOLLIN);
        pstTopo = &stTmp;
    }

    for(i = 0; i < g_stCfgInfo.uiTableNum; i++)
    {
        puiIndex = (UINT *)MEM_MALLOC(sizeof(UINT));
        if(puiIndex == NULL)
        {
            CW_SendErr("malloc error !!!");
            return TIMER_RUN_FOREVER;
        }
        *puiIndex = i;
        timer_register(i*1000, FALSE, (timer_func_t)PROXYC_CfgTimeout, puiIndex, NULL, "proxyc_cfg");
        puiIndex = NULL;
    }

    return TIMER_RUN_FOREVER;
}

int g_uiEpollFd = 0;

static void free_all_quit(void)
{
    PROXYC_FreeProxyTopo();
    close(g_uiEpollFd);
    exit(0);
}

void signal_process(int sig){
	switch(sig) {
	case SIGTERM:
	case SIGKILL:
	case SIGSEGV:
	case SIGSTOP:
	case SIGABRT:
	case SIGQUIT:
	case SIGINT:
		free_all_quit();
	}
}
static void signal_setup(void)
{
	signal(SIGUSR1, signal_process);
	signal(SIGUSR2, signal_process);
	signal(SIGTERM, signal_process);
	signal(SIGKILL, signal_process);
//	signal(SIGSEGV, signal_process);
	signal(SIGSTOP, signal_process);
	signal(SIGABRT, signal_process);
	signal(SIGQUIT, signal_process);
	signal(SIGINT, signal_process);
}

int main(int argc, char** argv)
{
    int uiManageFd;
    PROXYC_TOPO_INFO_T stTmp;
    int ret = 0;


    if(argc < 1)
    {
        return IPC_STATUS_OK;
    }
    PROXYC_CfgInit();

    ret = PROXYC_GetCfgInfo(argv[1], &g_stCfgInfo);
    if(IPC_STATUS_OK != ret)
    {
        CW_SendErr( "Get cfg from file(%s) error!!", argv[1]);
        return IPC_STATUS_OK;
    }

    if(g_stCfgInfo.pstTables == NULL)
    {
        CW_SendErr( "Get cfg from file(%s) error!!", argv[1]);
        return IPC_STATUS_OK;
    }
    PROXYC_PrintCfgInfo(&g_stCfgInfo);

    g_uiEpollFd = SOCKET_CreateEpoll();

	timer_init();
    timer_register(PROXYC_CONNECT_TIMEOUT*1000, TRUE, (timer_func_t)PROXYC_ConnectTimeout , &g_uiEpollFd, NULL, "proxyc_connect_timeout");

    while(SOCKET_EpollLoop(g_uiEpollFd, PROXYC_LoopReadDeal, NULL, PROXYC_LoopCloseDeal));

    return 0;
}


