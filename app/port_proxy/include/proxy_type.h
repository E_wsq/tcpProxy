

#ifndef PROXY_TYPE_H_
#define PROXY_TYPE_H_

typedef int (*EPOOL_LOOP_READ)(int uiEpollFd, UINT uiFd);
typedef int (*EPOOL_LOOP_CLOSE)(int uiEpollFd, UINT uiFd);
typedef int (*EPOOL_LOOP_WRITE)(int uiEpollFd, UINT uiFd);

typedef int (*PROXY_SOCKET_PKT_DEAL)(int uiEpollFd, void *pData, INT8 *aucPktBuf, int uiPktLen);

#define PROXY_EPOLL_MAX 1000
#define PROXY_EPOLL_TIMEOUT 1000


#define PROXY_SERVERPORT 8080   /*服务器监听端口号*/
#define SAMBA_PORT 139   /*服务器监听端口号*/

#define PROXY_LINK_CLIENT_TIMEOUT 60

#define PROXY_SYNC_TIMEOUT 1

#define PROXY_SOCK_MAX_PKT_LEN 4096
#define PROXY_CLIENT_SOCK_MAX_PKT_LEN 1280

typedef enum
{
    PROXY_PACKET_OP_MANAGE = 0,
    PROXY_PACKET_OP_DATA,
    PROXY_PACKET_OP_END
}PROXY_PACKET_OPRATION_T;

typedef enum
{
    PROXY_PKT_TYPE_TUNNEL_CFG = 0,  /* 服务代理 */
    PROXY_PKT_TYPE_TUNNEL_CFG_ACK,
    PROXY_PKT_TYPE_CONNECT,/* 客户端连接 */
    PROXY_PKT_TYPE_CONNECT_ACK,
    PROXY_PKT_TYPE_RELEASE,/* 客户端释放 */
    PROXY_PKT_TYPE_END
}PROXY_PACKET_TYPE_T;

typedef struct
{
    UINT8 aucMagic[6];
    UINT  uiPktLen;
    UINT8 ucOpration; /* PROXY_PACKET_OPRATION_T */
}__attribute__ ((__packed__)) PROXY_PACKET_HEAD_T;

typedef struct
{
    UINT uiLinkFd;
    UINT uiProxyFd;
}__attribute__ ((__packed__)) PROXY_PACKET_DATA_CTL_T;


typedef struct
{
    UINT16 usStatus;
    UINT16 uiLinkServerPort;
    UINT16 uiProxyfPort; /* 代理后的端口 */
    UINT32 uiProxyfIp; /* 代理后的端口 */
}PROXY_PACKET_MIRROR_T;

typedef struct
{
    UINT16 usStatus;
    UINT uiLinkClientFd;
    UINT uiLinkClientIp;
    UINT uiLinkClientPort;
    UINT16 uiLinkServerPort;
    UINT uiProxydFd; /* 代理后的端口 */
}PROXY_PACKET_LINK_REQUEST_T;

typedef struct
{
    UINT8 *s_LastPktBuf;
    int s_uiLastPktLen;
}PROXY_PACKET_BUFF_T;



extern INT8 g_szMagic[6];

#endif /* TIMER_H_ */
