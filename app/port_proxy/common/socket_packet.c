#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>

#include "private_type.h"
#include "private_log.h"
#include "proxy_type.h"
#include "btree/sys_btree.h"
#include "timer.h"

#include "cjson/cJSON.h"
#include "memory/sys_memory.h"


INT8 g_szMagic[6] = {0xf2, 0xe3, 0xa4, 0xd2, 0x1f, 0xe4};

int  PROXY_SocketGetLinkServerInfo
(
    IN cJSON *pstJson,
    OUT PROXY_PACKET_MIRROR_T *pstInfo
)
{
    UINT16 usProxyPort = 0;
    UINT16 usSrcPort = 0;
    cJSON *pstSub = NULL;

    pstSub = cJSON_GetObjectItem(pstJson, "Status");
    if (NULL != pstSub)
    {
        pstInfo->usStatus = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "LinkServerPort");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkServerPort = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ProxyfPort");
    if (NULL != pstSub)
    {
        pstInfo->uiProxyfPort = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ProxyfIp");
    if (NULL != pstSub)
    {
        pstInfo->uiProxyfIp = pstSub->valueint;
    }
    return IPC_STATUS_OK;
}

int  PROXY_SocketGetLinkAckInfo
(
    IN cJSON *pstJson,
    OUT PROXY_PACKET_LINK_REQUEST_T *pstInfo
)
{
    cJSON *pstSub = NULL;

    pstSub = cJSON_GetObjectItem(pstJson, "Status");
    if (NULL != pstSub)
    {
        pstInfo->usStatus = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientFd");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientFd = pstSub->valueint;
    }
    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientIp");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientIp = pstSub->valueint;
    }


    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientPort");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientPort= pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "LinkServerPort");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkServerPort = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ProxydFd");
    if (NULL != pstSub)
    {
        pstInfo->uiProxydFd = pstSub->valueint;
    }


    return IPC_STATUS_OK;
}

int PROXY_SocektGetReleaseInfo
(
    IN cJSON *pstJson,
    OUT PROXY_PACKET_LINK_REQUEST_T *pstInfo
)
{
    cJSON *pstSub = NULL;

    pstSub = cJSON_GetObjectItem(pstJson, "Status");
    if (NULL != pstSub)
    {
        pstInfo->usStatus = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientFd");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientFd = pstSub->valueint;
    }
    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientIp");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientIp = pstSub->valueint;
    }


    pstSub = cJSON_GetObjectItem(pstJson, "LinkClientPort");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkClientPort= pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "LinkServerPort");
    if (NULL != pstSub)
    {
        pstInfo->uiLinkServerPort = pstSub->valueint;
    }

    pstSub = cJSON_GetObjectItem(pstJson, "ProxydFd");
    if (NULL != pstSub)
    {
        pstInfo->uiProxydFd = pstSub->valueint;
    }


    return IPC_STATUS_OK;
}

void PKT_Dump(IN UINT8 *pucBuf, IN UINT uiSize)
{
    int i = 0;
    CW_SendDump("len=%d \n", uiSize);

    while(i < uiSize)
    {
        CW_SendDump("%02x ", pucBuf[i]);
        i++;
        if((i%16) == 0)
        {
            CW_SendDump("\n");
        }
    }
    CW_SendDump("\n");

    return;
}


int PROXY_PktSeekHead(INT8 *pucPktBuf, int uiPktLen)
{
    int i = 0;
    if(uiPktLen < sizeof(g_szMagic))
    {
        return uiPktLen;
    }

    for(i = 0; i <= (uiPktLen - sizeof(g_szMagic)); i++)
    {
        if(0 == memcmp(&pucPktBuf[i], g_szMagic, sizeof(g_szMagic)))
        {
            return i;
        }
    }

    return uiPktLen;
}

int PROXY_GetOnePkt(INT8 *pucPktBuf, int uiPktLen, int *puiUsedLen)
{
    PROXY_PACKET_HEAD_T *pstHead = NULL;
    UINT uiOnePktLen = 0;

    if(uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        return IPC_STATUS_NOT_FIND;
    }

    pstHead = (PROXY_PACKET_HEAD_T *)pucPktBuf;

    uiOnePktLen = ntohl(pstHead->uiPktLen);
    if(uiOnePktLen > uiPktLen)
    {
        return IPC_STATUS_NOT_FIND;
    }

    *puiUsedLen = uiOnePktLen;
    return IPC_STATUS_OK;
}

void PROXY_SocketRead(int uiEpollFd, int uiFd, void *pData, PROXY_PACKET_BUFF_T *pstPktBuf,PROXY_SOCKET_PKT_DEAL f_FuncDeal, EPOOL_LOOP_CLOSE f_FuncClose)
{
    UINT8 aucBuf[PROXY_SOCK_MAX_PKT_LEN];
    int uiLen = 0;
    UINT8 *pucPktBuf = NULL;
    int uiPktLen = 0;
    int uiHeadOffset = 0;

    uiLen = read(uiFd, aucBuf, PROXY_SOCK_MAX_PKT_LEN);
    if(uiLen <= 0)
    {
        SOCKET_DelEpoll(uiEpollFd, uiFd, f_FuncClose);
        return;
    }


    if(NULL == pstPktBuf->s_LastPktBuf)
    {
        pstPktBuf->s_uiLastPktLen = 0;
    }

    /* merge last buf */
    pucPktBuf = (UINT8 *)MEM_MALLOC(uiLen+pstPktBuf->s_uiLastPktLen);

    if(NULL == pucPktBuf)
    {
        CW_SendErr("malloc size(%d) error!!", uiLen+pstPktBuf->s_uiLastPktLen);
        return;
    }

    memset(pucPktBuf, 0, uiLen+pstPktBuf->s_uiLastPktLen);
    if(NULL != pstPktBuf->s_LastPktBuf)
    {
        memcpy(pucPktBuf, pstPktBuf->s_LastPktBuf, pstPktBuf->s_uiLastPktLen);
        uiPktLen = pstPktBuf->s_uiLastPktLen;

        MEM_FREE(pstPktBuf->s_LastPktBuf);
        pstPktBuf->s_LastPktBuf = NULL;
        pstPktBuf->s_uiLastPktLen = 0;
    }

    memcpy(&pucPktBuf[uiPktLen], aucBuf, uiLen);
    uiPktLen += uiLen;

    /* pkt too short */
    if(uiPktLen < sizeof(PROXY_PACKET_HEAD_T))
    {
        pstPktBuf->s_LastPktBuf = pucPktBuf;
        pstPktBuf->s_uiLastPktLen = uiPktLen;
        return;
    }

    uiHeadOffset = PROXY_PktSeekHead(pucPktBuf, uiPktLen);
    if(uiHeadOffset == uiPktLen)
    {
        /* 如果没有匹配到定帧字节 则只保留magic的长度 */
        if(uiPktLen <= sizeof(g_szMagic))
        {
            pstPktBuf->s_LastPktBuf = pucPktBuf;
            pstPktBuf->s_uiLastPktLen = uiPktLen;
        }
        else
        {
            pstPktBuf->s_LastPktBuf = (UINT8 *)MEM_MALLOC(sizeof(g_szMagic));
            memcpy(pstPktBuf->s_LastPktBuf, &pucPktBuf[uiPktLen-sizeof(g_szMagic)], sizeof(g_szMagic));
            pstPktBuf->s_uiLastPktLen = sizeof(g_szMagic);
            MEM_FREE(pucPktBuf);
        }
        return;
    }

    while(1)
    {
        INT8 *pucPktBufTmp = &pucPktBuf[uiHeadOffset];
        int uiPktLenTmp = uiPktLen - uiHeadOffset;
        int uiUsedLen = 0;
        int ret = 0;

        ret = PROXY_GetOnePkt(pucPktBufTmp, uiPktLenTmp, &uiUsedLen);
        if(IPC_STATUS_OK != ret)
        {
            pstPktBuf->s_LastPktBuf = (UINT8 *)MEM_MALLOC(uiPktLenTmp);

            memcpy(pstPktBuf->s_LastPktBuf, pucPktBufTmp, uiPktLenTmp);
            pstPktBuf->s_uiLastPktLen = uiPktLenTmp;

            MEM_FREE(pucPktBuf);
            return;
        }

        memcpy(aucBuf, pucPktBufTmp, uiUsedLen);
        f_FuncDeal(uiEpollFd, pData, aucBuf,uiUsedLen);

        uiHeadOffset += uiUsedLen;

        if(uiHeadOffset >= uiPktLen)
        {
            break;
        }
    }
    MEM_FREE(pucPktBuf);

    return;
}

