
DEBUG_DIR = debug

debug-build:
	@$(MAKE) -C $(DEBUG_DIR) \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		all
		
debug-install:
	@$(MAKE) -C $(DEBUG_DIR) \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		CONFIG_PREFIX="$(TARGETDIR)" \
		install


debug-clean:
	@$(MAKE) -C $(DEBUG_DIR) clean
	
